using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Plant.NET.Infrastructure;
using Plant.NET.Models;
using Plant.NET.Tests.Infrastructure;
using WireMock.RequestBuilders;
using WireMock.ResponseBuilders;
using WireMock.Server;
using Xunit;

namespace Plant.NET.Tests.Unit
{
	public class PlantNetClientTest : IDisposable
	{
		private readonly WireMockServer _mockServer;
		private readonly PlantNetClient _plantNetClient;

		public PlantNetClientTest()
		{
			_mockServer = WireMockServer.Start();

			var services = new ServiceCollection();
			services.AddPlantNetTestClient(_mockServer.Urls.First());
			var serviceProvider = services.BuildServiceProvider();
			_plantNetClient = serviceProvider.GetService<PlantNetClient>();
		}

		public void Dispose()
		{
			_mockServer?.Dispose();
		}

		[Fact]
		public async Task Identify_WhenPlantCanBeIdentified_ThenReturnsIdentificationResult()
		{
			var mockResponse = new IdentificationResult
			{
				Results = new List<PlantIdentificationResult>
				{
					new PlantIdentificationResult
						{ Score = 0, Species = new Species { ScientificNameWithoutAuthor = "Test" } }
				}
			};

			var body = JsonSerializer.Serialize(
				mockResponse,
				new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

			_mockServer
				.Given(
					Request.Create()
						.WithPath("*")
						.UsingAnyMethod())
				.RespondWith(
					Response.Create()
						.WithStatusCode(HttpStatusCode.OK)
						.WithBody(body));

			await using var stubImage = await Toolbox.LoadTestImage();
			var stubPlantImage = new PlantImage(stubImage);
			var result = (await _plantNetClient.Identify(stubPlantImage)).Results.First();

			AssertPlantIdentificationResultIsValid(result);

			var stubRemotePlantImage = new RemotePlantImage(string.Empty);
			result = (await _plantNetClient.Identify(stubRemotePlantImage)).Results.First();

			AssertPlantIdentificationResultIsValid(result);
		}

		[Fact]
		public async Task Identify_WhenPlantCanBeIdentifiedFromMultipleImages_ThenReturnsResults()
		{
			var mockResponse = new IdentificationResult
			{
				Results = new List<PlantIdentificationResult>
				{
					new PlantIdentificationResult
						{ Score = 0, Species = new Species { ScientificNameWithoutAuthor = "Test" } }
				}
			};

			var body = JsonSerializer.Serialize(
				mockResponse,
				new JsonSerializerOptions { PropertyNamingPolicy = JsonNamingPolicy.CamelCase });

			_mockServer
				.Given(
					Request.Create()
						.WithPath("*")
						.UsingAnyMethod())
				.RespondWith(
					Response.Create()
						.WithStatusCode(HttpStatusCode.OK)
						.WithBody(body));

			await using var image1 = await Toolbox.LoadTestImage();
			await using var image2 = await Toolbox.LoadTestImage();
			var stubPlantImages = new[]
			{
				new PlantImage(image1),
				new PlantImage(image2, PlantOrgan.Flower),
			};

			var result = (await _plantNetClient.Identify(stubPlantImages)).Results.First();

			AssertPlantIdentificationResultIsValid(result);

			var stubRemotePlantImages = new[]
			{
				new RemotePlantImage("TEST")
			};

			result = (await _plantNetClient.Identify(stubRemotePlantImages)).Results.First();

			AssertPlantIdentificationResultIsValid(result);
		}

		[Fact]
		public async Task Identify_WhenMoreThan5ImagesProvided_ThenThrows()
		{
			var stubPlantImages = Enumerable
				.Range(0, 6)
				.Select(_ => new PlantImage(null!))
				.ToList();

			var stubRemotePlantImages = Enumerable
				.Range(0, 6)
				.Select(_ => new RemotePlantImage(null!))
				.ToList();

			await Assert.ThrowsAsync<PlantNetException>(() => _plantNetClient.Identify(stubPlantImages));
			await Assert.ThrowsAsync<PlantNetException>(
				() => _plantNetClient.Identify(stubRemotePlantImages));
		}

		[Fact]
		public async Task Identify_WhenImageStreamIsInvalid_ThenThrows()
		{
			var stubMemoryStream = new MemoryStream();
			var stubPlantImage = new PlantImage(stubMemoryStream);

			await Assert.ThrowsAsync<ArgumentException>(
				() => _plantNetClient.Identify(stubPlantImage));

			await stubMemoryStream.DisposeAsync();

			await Assert.ThrowsAsync<ArgumentException>(
				() => _plantNetClient.Identify(stubPlantImage));
		}

		[Fact]
		public async Task Identify_WhenPlantCannotBeIdentified_ThenResultsListIsEmpty()
		{
			_mockServer
				.Given(
					Request.Create()
						.WithPath("*")
						.UsingPost())
				.RespondWith(
					Response.Create()
						.WithStatusCode(HttpStatusCode.NotFound));

			await using var stubImage = await Toolbox.LoadTestImage();
			var result = await _plantNetClient.Identify(new PlantImage(stubImage));

			Assert.Empty(result.Results);

			result = await _plantNetClient.Identify(new RemotePlantImage(""));

			Assert.Empty(result.Results);
		}

		private void AssertPlantIdentificationResultIsValid(PlantIdentificationResult plantIdResult)
		{
			Assert.NotNull(plantIdResult.Species);
			Assert.Equal(0, plantIdResult.Score);
			Assert.Equal("Test", plantIdResult.Species.ScientificNameWithoutAuthor);
		}
	}
}
