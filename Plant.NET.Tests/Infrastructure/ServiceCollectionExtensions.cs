using System.Reflection;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Plant.NET.Infrastructure;

namespace Plant.NET.Tests.Infrastructure
{
	public static class ServiceCollectionExtensions
	{
		public static void AddPlantNetTestClient(
			this IServiceCollection services,
			string mockHttpClientBaseAddress)
		{
			var builder = new ConfigurationBuilder()
				.AddUserSecrets(Assembly.GetCallingAssembly())
				.AddEnvironmentVariables();

			var configuration = builder.Build();
			services.Configure<PlantNetClientOptions>(
				plantNetOptions =>
				{
					plantNetOptions.ApiKey = configuration["PlantNet:ApiKey"] ?? Toolbox.TestApiKey;
					plantNetOptions.ApiUrl = mockHttpClientBaseAddress;
				});

			services.AddPlantNetHttpClient();
		}
	}
}
