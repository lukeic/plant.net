using System;
using System.IO;
using System.Threading.Tasks;

namespace Plant.NET.Tests.Infrastructure
{
	public static class Toolbox
	{
		public const string TestApiKey = "TESTKEY";

		public const bool IsPlantNetIntegrationTestingEnabled = false;

		public static async Task<MemoryStream> LoadTestImage()
		{
			var imagePath = Path.Combine(Environment.CurrentDirectory, "Infrastructure", "plant.jpg");
			await using var imageFile = File.OpenRead(imagePath);
			var result = new MemoryStream();
			await imageFile.CopyToAsync(result);

			return result;
		}
	}
}
