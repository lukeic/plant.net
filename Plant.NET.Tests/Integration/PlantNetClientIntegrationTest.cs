using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using Plant.NET.Infrastructure;
using Plant.NET.Models;
using Plant.NET.Tests.Infrastructure;
using Xunit;

namespace Plant.NET.Tests.Integration
{
	public class PlantNetClientIntegrationTest : IDisposable
	{
		private readonly PlantNetClient _plantNetClient;

		public PlantNetClientIntegrationTest()
		{
			var services = new ServiceCollection();
			services.AddPlantNetTestClient(PlantNetConstants.ApiUrl);
			var serviceProvider = services.BuildServiceProvider();
			_plantNetClient = serviceProvider.GetService<PlantNetClient>();
		}

		public void Dispose()
		{
		}

		[Fact]
		public async Task Identify_WhenPlantCanBeIdentified_ThenReturnsIdentificationResult()
		{
			if (!Toolbox.IsPlantNetIntegrationTestingEnabled)
			{
				return;
			}

			await using var stubImage = await Toolbox.LoadTestImage();
			var result = await _plantNetClient.Identify(new PlantImage(stubImage));

			Assert.NotEmpty(result.Results);

			var stubRemotePlantImages = new[]
			{
				new RemotePlantImage(
					"https://my.plantnet.org/public/media/image_1.jpeg",
					PlantOrgan.Flower
				),
				new RemotePlantImage("https://my.plantnet.org/public/media/image_2.jpeg"),
			};

			result = await _plantNetClient.Identify(stubRemotePlantImages);

			Assert.NotEmpty(result.Results);
			Assert.Contains(
				result.Results,
				_ => _.Species.ScientificNameWithoutAuthor == "Hibiscus rosa-sinensis");
		}

		[Fact]
		public async Task Identify_WhenImageInputIsInvalid_ThenThrows()
		{
			if (!Toolbox.IsPlantNetIntegrationTestingEnabled)
			{
				return;
			}

			await using var stubMemoryStream = new MemoryStream(new byte[] { 0 });
			await Assert.ThrowsAsync<PlantNetInvalidInputException>(
				() => _plantNetClient.Identify(new PlantImage(stubMemoryStream)));
		}
	}
}
