# Plant.NET
Plant.NET is an API client for the [Pl@ntNet](https://plantnet.org/en/) plant identification [API](https://my-api.plantnet.org/#/). It supports identification of both in-memory and hosted images of plants.

## How to Use
Usage is simple. 

`PlantNetClient` exposes only one method, `Identify()`. A plant can be identified from either a single image, or from multiple images (max. 5) of different parts of the same plant.

Images must be represented as streams or URLs. Mixing local and remote images is not supported.

### Quick-Start
```c#
using Plant.NET;

public void ConfigureServices(IServiceCollection services)
{
    // Register PlantNetClient
    services.AddPlantNetClient(options => options.ApiKey = "YourApiKey");
}
```

```c#
using Plant.NET;
using Plant.NET.Models;

public class PlantIdentificationService
{
    private PlantNetClient _plantNet;
    
    public PlantIdentificationService(PlantNetClient plantNet)
    {
        _plantNet = plantNet;
    }
    
    public async Task IdentifyPlant(string imageUrl)
    {
        // Create a new RemotePlantImage with an optional PlantOrgan tag (defaults to PlantOrgan.Leaf)
        var plantImage = new RemotePlantImage(imageUrl, PlantOrgan.Leaf);

        // Get a list of possible plant species
        var result = await _plantNet.Identify(plantImage);
    }
}
```

### Configuration Options
```c#
public class PlantNetClientOptions
{
    public string ApiKey { get; set; } = string.Empty;

    public int NumRequestRetries { get; set; } = PlantNetClientDefaults.NumRequestRetries;
}
```
