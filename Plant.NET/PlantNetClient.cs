using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Runtime.CompilerServices;
using System.Text.Json;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Options;
using Plant.NET.Infrastructure;
using Plant.NET.Models;

[assembly: InternalsVisibleTo("Plant.NET.Tests")]

namespace Plant.NET
{
	/// <summary>
	///  A client for Pl@ntNet's plant identification API.
	/// </summary>
	public class PlantNetClient
	{
		private readonly PlantNetClientOptions _clientOptions;
		private readonly HttpClient _httpClient;

		public PlantNetClient(HttpClient httpClient, IOptions<PlantNetClientOptions> options)
		{
			_httpClient = httpClient;
			_clientOptions = options.Value;
		}

		/// <summary>
		/// Identifies a plant from an image of either its flowers, leaves, bark or fruit.
		/// </summary>
		/// <param name="plantImage"><see cref="PlantImage"/></param>
		/// <returns>
		/// A list of likely candidates for the plant along with a corresponding score.
		/// <see cref="IdentificationResult"/>
		/// </returns>
		/// <exception cref="PlantNetInvalidInputException">Thrown if PlantNet's API returns a 400.</exception>
		/// <exception cref="PlantNetServerException">Thrown if PlantNet's API returns a 500.</exception>
		/// <exception cref="PlantNetException">Thrown for any general runtime errors.</exception>
		public Task<IdentificationResult> Identify(PlantImage plantImage)
		{
			return Identify(new[] { plantImage });
		}

		/// <summary>
		/// Identifies a plant from multiple images of its flowers, leaves, bark or fruit.
		/// <para>See <seealso cref="PlantOrgan"/> for supported plant organs.</para>
		/// </summary>
		/// <param name="plantImages">
		/// A list of images of a plant's different organs. A maximum of 5 images is allowed.
		/// <see cref="PlantImage"/>
		/// </param>
		/// <returns>
		/// A list of likely candidates for the plant along with a corresponding score.
		/// <see cref="IdentificationResult"/>
		/// </returns>
		public async Task<IdentificationResult> Identify(IList<PlantImage> plantImages)
		{
			AssertImageCountLimit(plantImages);

			var body = new MultipartFormDataContent();
			foreach (var plantImage in plantImages)
			{
				AddPlantImageToBody(plantImage, body);
			}

			var result = await PostRequest<IdentificationResult>("identify/all", body);
			foreach (var plantImage in plantImages)
			{
				await plantImage.Image.DisposeAsync();
			}

			return result;
		}

		/// <inheritdoc cref="Identify(Plant.NET.Models.PlantImage)"/>
		public Task<IdentificationResult> Identify(RemotePlantImage plantImage)
		{
			return Identify(new[] { plantImage });
		}

		/// <inheritdoc cref="Identify(IList{Plant.NET.Models.PlantImage})"/>
		public async Task<IdentificationResult> Identify(IList<RemotePlantImage> plantImages)
		{
			AssertImageCountLimit(plantImages);

			var query = GetNewQuery();
			foreach (var (image, organ) in plantImages)
			{
				query.Add("images", image);
				query.Add("organs", organ.GetName());
			}

			var result = await GetRequest<IdentificationResult>("identify/all", query.ToString());

			return result;
		}

		private void AssertImageCountLimit<T>(IList<T> plantImages)
		{
			if (plantImages.Count > PlantNetConstants.MaxImagesPerRequest)
			{
				throw new PlantNetException(
					$"A maximum of {PlantNetConstants.MaxImagesPerRequest} plant images is allowed");
			}
		}

		private void AddPlantImageToBody(
			PlantImage plantImage,
			MultipartFormDataContent body)
		{
			var (image, organ) = plantImage;
			if (!image.CanRead)
			{
				throw new ArgumentException("Stream cannot be read", nameof(image));
			}

			if (image.Length == 0)
			{
				throw new ArgumentException("Stream is empty", nameof(image));
			}

			image.Position = 0;
			body.Add(
				new StreamContent(image),
				"images",
				Guid.NewGuid()
					.ToString());

			body.Add(new StringContent(organ.GetName()), "organs");
		}

		private QueryBuilder GetNewQuery()
		{
			var query = new QueryBuilder();
			query.Add(PlantNetConstants.ApiKeyHeader, _clientOptions.ApiKey);

			return query;
		}

		private async Task<TResponse> GetRequest<TResponse>(string uri, string query)
			where TResponse : new()
		{
			var httpResponse = await _httpClient.GetAsync($"{uri}{query}");

			return await TryDeserialiseResponse<TResponse>(httpResponse);
		}

		private async Task<TResponse> PostRequest<TResponse>(string uri, HttpContent body)
			where TResponse : new()
		{
			var query = GetNewQuery();
			var httpResponse = await _httpClient.PostAsync($"{uri}{query}", body);

			return await TryDeserialiseResponse<TResponse>(httpResponse);
		}

		private async Task<TResponse> TryDeserialiseResponse<TResponse>(HttpResponseMessage httpResponse)
			where TResponse : new()
		{
			try
			{
				httpResponse.EnsureSuccessStatusCode();
			}
			catch (HttpRequestException e)
			{
				switch (httpResponse.StatusCode)
				{
					case HttpStatusCode.NotFound:
						return new TResponse();
					case HttpStatusCode.BadRequest:
						var error = await httpResponse.Content.ReadAsStringAsync();
						error += "\\s";
						error += await httpResponse.RequestMessage.Content.ReadAsStringAsync();
						throw new PlantNetInvalidInputException(error);
					default:
						throw new PlantNetServerException($"API call to Pl@ntNet failed: {e.Message}");
				}
			}

			var responseStream = await httpResponse.Content.ReadAsStreamAsync();
			TResponse result;
			try
			{
				result = await JsonSerializer.DeserializeAsync<TResponse>(
					responseStream,
					new JsonSerializerOptions { PropertyNameCaseInsensitive = true });
			}
			catch (JsonException e)
			{
				throw new PlantNetException(
					$"Failed to deserialise response body into type {typeof(TResponse)}: {e.Message}");
			}

			return result;
		}
	}
}
