using System;

namespace Plant.NET.Infrastructure
{
	public class PlantNetServerException : PlantNetException
	{
		public PlantNetServerException(string? message) : base(message)
		{
		}

		public PlantNetServerException(string? message, Exception? innerException) : base(
			message,
			innerException)
		{
		}
	}
}
