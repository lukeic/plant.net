using System;

namespace Plant.NET.Infrastructure
{
	public class PlantNetException : Exception
	{
		public PlantNetException(string? message) : base(message)
		{
		}

		public PlantNetException(string? message, Exception? innerException) : base(message, innerException)
		{
		}
	}
}
