using System;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Options;
using Polly;
using Polly.Extensions.Http;

namespace Plant.NET.Infrastructure
{
	public static class ServiceCollectionExtensions
	{
		/// <summary>
		/// Sets up and provides access to a Pl@ntNet API client.
		/// </summary>
		public static void AddPlantNetClient(
			this IServiceCollection services,
			Action<PlantNetClientOptions> options)
		{
			services.Configure(options);
			services.AddPlantNetHttpClient();
		}

		internal static void AddPlantNetHttpClient(this IServiceCollection services)
		{
			var options = services.GetPlantNetOptions();
			if (string.IsNullOrWhiteSpace(options.ApiKey))
			{
				throw new PlantNetException("A Pl@ntNet API key must be supplied.");
			}

			services
				.AddHttpClient<PlantNetClient>(client => { client.BaseAddress = new Uri(options.ApiUrl); })
				.AddPolicyHandler(
					message => HttpPolicyExtensions
						.HandleTransientHttpError()
						.WaitAndRetryAsync(
							options.NumRequestRetries,
							retryAttempt => TimeSpan.FromSeconds(Math.Pow(2, retryAttempt))));
		}

		internal static PlantNetClientOptions GetPlantNetOptions(this IServiceCollection services)
		{
			return services
				.BuildServiceProvider()
				.GetRequiredService<IOptions<PlantNetClientOptions>>()
				.Value;
		}
	}
}
