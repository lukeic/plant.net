namespace Plant.NET.Infrastructure
{
	internal static class PlantNetConstants
	{
		public const string ApiUrl = "https://my-api.plantnet.org/v2/";

		public const string ApiKeyHeader = "api-key";

		public const int MaxImagesPerRequest = 5;
	}
}
