using System.Collections.Generic;
using Plant.NET.Models;

namespace Plant.NET.Infrastructure
{
	public static class PlantNetClientDefaults
	{
		/// <summary>
		/// Number of times the PlantNetClient should retry failed requests.
		/// </summary>
		public const int NumRequestRetries = 3;

		/// <summary>
		/// The default plant organ to match plant pictures against.
		/// </summary>
		public const PlantOrgan Organ = PlantOrgan.Leaf;

		/// <summary>
		/// A default list of plant organs to match
		/// </summary>
		public static readonly IList<PlantOrgan> Organs = new[] { PlantOrgan.Leaf };
	}
}
