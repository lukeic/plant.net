using System;

namespace Plant.NET.Infrastructure
{
	public class PlantNetInvalidInputException : PlantNetException
	{
		public PlantNetInvalidInputException(string? message) : base(message)
		{
		}

		public PlantNetInvalidInputException(string? message, Exception? innerException) : base(
			message,
			innerException)
		{
		}
	}
}
