using System.Collections.Generic;

namespace Plant.NET.Models
{
	public class Species : Taxon
	{
		public Taxon Genus { get; set; } = null!;

		public Taxon Family { get; set; } = null!;

		public List<string> CommonNames { get; set; } = null!;
	}
}
