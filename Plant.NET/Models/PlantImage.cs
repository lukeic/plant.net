using System.IO;
using Plant.NET.Infrastructure;

namespace Plant.NET.Models
{
	/// <summary>
	/// An in-memory image of a plant to identify, with a tag specifying which plant organ the image is of.
	/// </summary>
	public class PlantImage : PlantImageBase<Stream>
	{
		public PlantImage(Stream image, PlantOrgan organ = PlantNetClientDefaults.Organ) : base(image, organ)
		{
		}
	}
}
