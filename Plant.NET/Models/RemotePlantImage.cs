using Plant.NET.Infrastructure;

namespace Plant.NET.Models
{
	/// <summary>
	/// A remotely-hosted image of a plant to identify, with a tag specifying which plant organ the image is of.
	/// </summary>
	public class RemotePlantImage : PlantImageBase<string>
	{
		public RemotePlantImage(string image, PlantOrgan organ = PlantNetClientDefaults.Organ) : base(
			image,
			organ)
		{
		}
	}
}
