namespace Plant.NET.Models
{
	public class Taxon
	{
		public string ScientificNameWithoutAuthor { get; set; } = null!;

		public string ScientificNameAuthorship { get; set; } = null!;
	}
}
