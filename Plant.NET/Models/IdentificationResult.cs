using System.Collections.Generic;
using System.Text.Json.Serialization;

namespace Plant.NET.Models
{
	/// <summary>
	/// Pl@ntNet's response to sending a plant identification request. Contains information about the original request and a list of most-likely plant species.
	/// </summary>
	public class IdentificationResult
	{
		/// <summary>
		/// Details of the original request sent to Pl@ntNet.
		/// </summary>
		public Query Query { get; set; } = null!;

		/// <summary>
		/// A list of likely plant species with a possibility score.
		/// <para>See <seealso cref="PlantIdentificationResult"/>.</para>
		/// </summary>
		public List<PlantIdentificationResult> Results { get; set; } = new List<PlantIdentificationResult>();

		public string Language { get; set; } = null!;

		/// <summary>
		/// Property is currently misspelt as 'prefered' in Pl@ntNet's API.
		/// </summary>
		[JsonPropertyName("preferedReferential")]
		public string PreferredReferential { get; set; } = null!;

		public string SwitchToProject { get; set; } = null!;

		/// <summary>
		/// Number of remaining requests available on the API key used.
		/// </summary>
		public uint RemainingIdentificationRequests { get; set; }
	}
}
