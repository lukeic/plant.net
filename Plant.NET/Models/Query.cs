using System.Collections.Generic;

namespace Plant.NET.Models
{
	public class Query
	{
		public string Project { get; set; } = null!;

		public List<string> Images { get; set; } = null!;

		public List<string> Organs { get; set; } = null!;
	}
}
