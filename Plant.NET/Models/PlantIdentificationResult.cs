namespace Plant.NET.Models
{
	public class PlantIdentificationResult
	{
		public float Score { get; set; }

		public Species Species { get; set; } = null!;

		/// <summary>
		/// GBIF (Global Biodiversity Information Facility) data. Can be used with the GBIF API to retrieve more information about the plant.
		/// </summary>
		public GbifData? Gbif { get; set; }
	}
}
