using System.Diagnostics.CodeAnalysis;
using Plant.NET.Infrastructure;

namespace Plant.NET.Models
{
	public abstract class PlantImageBase<T>
	{
		public PlantImageBase(T image, PlantOrgan organ = PlantNetClientDefaults.Organ)
		{
			Image = image;
			Organ = organ;
		}

		public void Deconstruct(out T image, out PlantOrgan organ)
		{
			image = Image;
			organ = Organ;
		}

		/// <summary>
		/// An image of a plant. Either a stream or a URL.
		/// <para>
		/// See <see cref="PlantImage"/> and <see cref="RemotePlantImage"/>
		/// </para>
		/// </summary>
		public T Image { get; }

		/// <summary>
		/// The plant organ that the picture is of.
		/// </summary>
		public PlantOrgan Organ { get; }
	}
}
